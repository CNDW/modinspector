/* eslint-env node */

const WebSocket = require('ws');
const parseData = require('./parse');

const GAME_INSTANCE = 'instance';
const INSPECTOR = 'inspector';
const REGISTER_TYPES = [INSPECTOR, GAME_INSTANCE];

const EXP = 5 * 60 *1000; // 5 minutes

function purgeOldSockets(wsServer) {
    const now = new Date();
    wsServer.clients.forEach((s) => {
      if (!s.birth) s.birth = now;

      if ((s.birth - now) > EXP && !REGISTER_TYPES.includes(s.identity)) {
        try {
          s.terminate();
        } catch (err) {
          console.error(err);
        }
      }
    });
}

module.exports = function injectWebsocketServer({ app, options }) {
  const wsServer = new WebSocket.Server({ port: 40510 });
  wsServer.activeUsers = {};

  function getTarget(targetName) {
    const target = wsServer.activeUsers[targetName];
    if (!target) {
      throw new Error(`Unable to process handler, no ${targetName} connected`);
    }

    return target;
  }

  wsServer.on('error', err => console.error(err));

  wsServer.on('connection', (socket) => {
    console.log(`new socket recieved at ${new Date().toString()}`);
    socket.identity = null;
    socket.birth = new Date();

    purgeOldSockets(wsServer);

    socket.on('error', (err) => {
      console.log(`error on socket ${socket.identity}`, err);
      socket.terminate();
      wsServer.activeUsers[socket.identity] = null;
    });

    socket.on('message', function (data) {
      const [ command, payload, targetName = INSPECTOR ] = parseData(data);

      if (command === 'noop') {
        console.log(`NOOP Message recieved: ${data}`);
        return;
      }

      if (command === 'register') {
        if (!REGISTER_TYPES.includes(payload.user)){
          console.log('invalid user registration: ', payload.user);
        } else {

          wsServer.clients.forEach(s => {
            if (s.identity === payload.user) s.terminate();
          });
          socket.identity = payload.user;
          wsServer.activeUsers[payload.user] = socket;
          console.log('new user registry complete for', payload.user);
        }
        return;
      }

      try {
        const target = getTarget(targetName);

        target.send(JSON.stringify([command, payload]), (err) => {
          if (err) {
            throw new Error(`unable to send ${command} to ${targetName}`);
          }
        });

      } catch (err) {
        console.error(err);
      }

    });
  });
};
