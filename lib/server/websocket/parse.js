/* eslint-env node */

const NOOP = ['noop'];
module.exports = function parseData(data) {
  try {
    let [cmdString, payload] = JSON.parse(data);

    let [target, command ] = cmdString.split(':');

    if (!command) {
      command = target;
      target = undefined;
    }

    return [command, payload, target];

  } catch (err) {
    console.log(`unable to parse recieved message: ${data}`);

    return NOOP;
  }
};
