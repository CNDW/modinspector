/* eslint-env node */
'use strict';

const injectWebsocketServer = require('./websocket');

module.exports = {
  name: require('./package').name,

  serverMiddleware: injectWebsocketServer
};
