import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  websocket: service(),
  store: service(),

  actions: {
    getScenes() {
      this.store.unloadAll('scene');
      this.websocket.send('getScenes');
    }
  }
});
