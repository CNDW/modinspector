import DS from 'ember-data';

export default DS.JSONAPISerializer.extend({
  keyForAttribute(attr, method) {
    return attr;
  },

  fromSocket(payload, store) {

    return {
      id: payload.attributes.path,
      attributes: Object.assign({ unityType: payload.type }, payload.attributes),
      type: 'scene',
      relationships: {
        "children": {
          data: (payload.children || []).map(c => ({ id: c.path, type: 'game-object' }))
        }
      }
    };
  }
});
