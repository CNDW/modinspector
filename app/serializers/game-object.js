import DS from 'ember-data';

export default DS.JSONAPISerializer.extend({
  keyForAttribute(attr, method) {
    return attr;
  },

  fromSocket(payload, parent = null) {
    const data = {
      id: payload.path,
      attributes: Object.assign({ unityType: payload.type }, payload.attributes),
      type: 'game-object',
      relationships: {
        "children": {
          data: (payload.children || []).map(c => ({ id: c.path, type: 'game-object' }))
        }
      }
    };

    if (parent) {
      data.relationships.parent = { data: { type: 'game-object', id: parent } };
    }

    return data;
  },

  fromSceneSocket(payload) {
    const mapChildren = (objects) => {
      const data = objects.map(o => this.fromSocket(o));

      const children = objects
        .map(o => mapChildren(o.children, o))
        .reduce((m, list) => m.concat(list), []);

      return data.concat(children);
    };

    return mapChildren(payload.children);
  }
});
