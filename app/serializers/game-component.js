import DS from 'ember-data';

export default DS.JSONAPISerializer.extend({
  keyForAttribute(attr, method) {
    return attr;
  },

  fromSocket(payload) {
    const data = {
      id: `${payload.gameObject}::${payload.name}`,
      attributes: {
        unityType: payload.type,
        name: payload.name,
        fields: payload.attributes
      },
      type: 'game-component',
      relationships: {
        "game-object": { data: { type: 'game-object', id: payload.gameObject } }
      }
    };

    return data;
  },
});
