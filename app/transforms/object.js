import DS from 'ember-data';

import isPlainObject from 'lodash.isplainobject';

export default DS.Transform.extend({
  deserialize: function(value) {
    if (!isPlainObject(value)) {
      return {};
    } else {
      return value;
    }
  },
  serialize: function(value) {
    if (!isPlainObject(value)) {
      return {};
    } else {
      return value;
    }
  }
});
