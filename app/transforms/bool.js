import DS from 'ember-data';

const TRUE = /true/i;
const FALSE = /false/i;

export default DS.Transform.extend({
  deserialize(serialized) {
    if (typeof serialize !== 'string') {
      return !!serialized;
    } else if (TRUE.test(serialized)) {
      return true;
    } else if (FALSE.test(serialized)) {
      return false;
    } else {
      return !!serialized;
    }
  },

  serialize(deserialized) {
    return deserialized ? 'True' : 'False';
  }
});
