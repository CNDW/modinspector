import DS from 'ember-data';

import { computed } from '@ember/object';

export default DS.Model.extend({
  "unityType": DS.attr('string'),
  "path": DS.attr('string'),
  "name": DS.attr('string'),
  "isLoaded": DS.attr('bool'),
  "buildIndex": DS.attr('number'),
  "isDirty": DS.attr('bool'),
  "rootCount": DS.attr('number'),

  "children": DS.hasMany('game-object'),
  isActiveScene: DS.attr('bool', { defaultValue: false }),

  hasNoChidren: computed.empty('children'),

  isRefreshing: false,

  didLoad() {
    this.set('isRefreshing', false);
  }
});
