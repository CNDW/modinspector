import DS from 'ember-data';

export default DS.Model.extend({
  "unityType": DS.attr('string'),
  name: DS.attr('string'),

  fields: DS.attr('object'),

  gameObject: DS.belongsTo('game-object')
});
