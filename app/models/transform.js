import DS from 'ember-data';
import { computed } from '@ember/object';

function positionString(p = {}) {
  return `(${p.x}, ${p.y}, ${p.z}) magnitude: ${p.magnitude}, sqrMagnitude: ${p['sqr-magnitude']}`;
}

export default DS.Model.extend({
  forward: DS.attr('string'),
  localRotation: DS.attr('string'),
  localScale: DS.attr('string'),
  right: DS.attr('string'),
  rotation: DS.attr('string'),
  up: DS.attr('string'),

  position: DS.attr('object'),
  localPosition: DS.attr('object'),

  gameObject: DS.belongsTo('game-object'),

  positionString: computed('position', function() {
    return positionString(this.get('position'));
  }),

  localPositionString: computed('localPosition', function() {
    return positionString(this.get('localPosition'));
  })
});
