import DS from 'ember-data';

import { computed } from '@ember/object';

export default DS.Model.extend({
  "unityType": DS.attr('string'),
  "layer": DS.attr('number'),
  "active": DS.attr('bool'),
  "activeSelf": DS.attr('bool'),
  "activeInHierarchy": DS.attr('bool'),
  "isStatic": DS.attr('bool'),
  "tag": DS.attr('string'),
  "scene": DS.attr('string'),
  "gameObject": DS.attr('string'),
  "name": DS.attr('string'),
  "hideFlags": DS.attr('string'),

  "children": DS.hasMany('game-object', { inverse: 'parent' }),
  "parent": DS.belongsTo('game-object', { inverse: 'children' }),
  "components": DS.hasMany('game-component', { async: true }),

  transform: DS.belongsTo('transform'),

  hasNoChildren: computed.empty('children'),

  isRefreshing: false,

  didLoad() {
    this.set('isRefreshing', false);
  }
});
