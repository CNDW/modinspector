export default {
  "scene": {
    "type": "UnityEngine.SceneManagement.Scene",
    "attributes": {
      "isActiveScene": "False",
      "path": "Assets/Scenes/MenuEnvironment.unity",
      "name": "MenuEnvironment",
      "isLoaded": "True",
      "buildIndex": "7",
      "isDirty": "False",
      "rootCount": "7"
    },
    "children": [
      {
        "path": "/Hightlight Light",
        "type": "UnityEngine.GameObject",
        "attributes": {
          "transform": "Hightlight Light (UnityEngine.Transform)",
          "layer": "0",
          "active": "True",
          "activeSelf": "True",
          "activeInHierarchy": "True",
          "isStatic": "False",
          "tag": "Untagged",
          "scene": "Assets/Scenes/MenuEnvironment.unity",
          "gameObject": "Hightlight Light (UnityEngine.GameObject)",
          "name": "Hightlight Light",
          "hideFlags": "None"
        },
        "children": []
      },
      {
        "path": "/uSkyManager",
        "type": "UnityEngine.GameObject",
        "attributes": {
          "transform": "uSkyManager (UnityEngine.Transform)",
          "layer": "0",
          "active": "True",
          "activeSelf": "True",
          "activeInHierarchy": "True",
          "isStatic": "False",
          "tag": "Untagged",
          "scene": "Assets/Scenes/MenuEnvironment.unity",
          "gameObject": "uSkyManager (UnityEngine.GameObject)",
          "name": "uSkyManager",
          "hideFlags": "None"
        },
        "children": [
          {
            "path": "/uSkyManager/sunLensFlarePos",
            "type": "UnityEngine.GameObject",
            "attributes": {
              "transform": "sunLensFlarePos (UnityEngine.Transform)",
              "layer": "0",
              "active": "True",
              "activeSelf": "True",
              "activeInHierarchy": "True",
              "isStatic": "False",
              "tag": "Untagged",
              "scene": "Assets/Scenes/MenuEnvironment.unity",
              "gameObject": "sunLensFlarePos (UnityEngine.GameObject)",
              "name": "sunLensFlarePos",
              "hideFlags": "None"
            },
            "children": []
          },
          {
            "path": "/uSkyManager/sunLensFlare",
            "type": "UnityEngine.GameObject",
            "attributes": {
              "transform": "sunLensFlare (UnityEngine.Transform)",
              "layer": "0",
              "active": "False",
              "activeSelf": "False",
              "activeInHierarchy": "False",
              "isStatic": "False",
              "tag": "Untagged",
              "scene": "Assets/Scenes/MenuEnvironment.unity",
              "gameObject": "sunLensFlare (UnityEngine.GameObject)",
              "name": "sunLensFlare",
              "hideFlags": "None"
            },
            "children": []
          }
        ]
      },
      {
        "path": "/Directional Light",
        "type": "UnityEngine.GameObject",
        "attributes": {
          "transform": "Directional Light (UnityEngine.Transform)",
          "layer": "0",
          "active": "True",
          "activeSelf": "True",
          "activeInHierarchy": "True",
          "isStatic": "False",
          "tag": "Untagged",
          "scene": "Assets/Scenes/MenuEnvironment.unity",
          "gameObject": "Directional Light (UnityEngine.GameObject)",
          "name": "Directional Light",
          "hideFlags": "None"
        },
        "children": []
      },
      {
        "path": "/Waterscape",
        "type": "UnityEngine.GameObject",
        "attributes": {
          "transform": "Waterscape (UnityEngine.Transform)",
          "layer": "0",
          "active": "True",
          "activeSelf": "True",
          "activeInHierarchy": "True",
          "isStatic": "False",
          "tag": "Untagged",
          "scene": "Assets/Scenes/MenuEnvironment.unity",
          "gameObject": "Waterscape (UnityEngine.GameObject)",
          "name": "Waterscape",
          "hideFlags": "None"
        },
        "children": []
      },
      {
        "path": "/logo",
        "type": "UnityEngine.GameObject",
        "attributes": {
          "transform": "logo (UnityEngine.Transform)",
          "layer": "8",
          "active": "True",
          "activeSelf": "True",
          "activeInHierarchy": "True",
          "isStatic": "False",
          "tag": "Untagged",
          "scene": "Assets/Scenes/MenuEnvironment.unity",
          "gameObject": "logo (UnityEngine.GameObject)",
          "name": "logo",
          "hideFlags": "None"
        },
        "children": [
          {
            "path": "/logo/subnautica_logo(Clone)",
            "type": "UnityEngine.GameObject",
            "attributes": {
              "transform": "subnautica_logo(Clone) (UnityEngine.Transform)",
              "layer": "8",
              "active": "True",
              "activeSelf": "True",
              "activeInHierarchy": "True",
              "isStatic": "False",
              "tag": "Untagged",
              "scene": "Assets/Scenes/MenuEnvironment.unity",
              "gameObject": "subnautica_logo(Clone) (UnityEngine.GameObject)",
              "name": "subnautica_logo(Clone)",
              "hideFlags": "None"
            },
            "children": []
          }
        ]
      },
      {
        "path": "/MarmoSkies",
        "type": "UnityEngine.GameObject",
        "attributes": {
          "transform": "MarmoSkies (UnityEngine.Transform)",
          "layer": "0",
          "active": "True",
          "activeSelf": "True",
          "activeInHierarchy": "True",
          "isStatic": "False",
          "tag": "Untagged",
          "scene": "Assets/Scenes/MenuEnvironment.unity",
          "gameObject": "MarmoSkies (UnityEngine.GameObject)",
          "name": "MarmoSkies",
          "hideFlags": "None"
        },
        "children": [
          {
            "path": "/MarmoSkies/SkySafeShallows(Clone)",
            "type": "UnityEngine.GameObject",
            "attributes": {
              "transform": "SkySafeShallows(Clone) (UnityEngine.Transform)",
              "layer": "0",
              "active": "True",
              "activeSelf": "True",
              "activeInHierarchy": "True",
              "isStatic": "False",
              "tag": "Untagged",
              "scene": "Assets/Scenes/MenuEnvironment.unity",
              "gameObject": "SkySafeShallows(Clone) (UnityEngine.GameObject)",
              "name": "SkySafeShallows(Clone)",
              "hideFlags": "None"
            },
            "children": []
          }
        ]
      },
      {
        "path": "/EnvironmentCamera",
        "type": "UnityEngine.GameObject",
        "attributes": {
          "transform": "EnvironmentCamera (UnityEngine.Transform)",
          "layer": "0",
          "active": "True",
          "activeSelf": "True",
          "activeInHierarchy": "True",
          "isStatic": "False",
          "tag": "Untagged",
          "scene": "Assets/Scenes/MenuEnvironment.unity",
          "gameObject": "EnvironmentCamera (UnityEngine.GameObject)",
          "name": "EnvironmentCamera",
          "hideFlags": "None"
        },
        "children": [
          {
            "path": "/EnvironmentCamera/Main Camera",
            "type": "UnityEngine.GameObject",
            "attributes": {
              "transform": "Main Camera (UnityEngine.Transform)",
              "layer": "0",
              "active": "True",
              "activeSelf": "True",
              "activeInHierarchy": "True",
              "isStatic": "False",
              "tag": "MainCamera",
              "scene": "Assets/Scenes/MenuEnvironment.unity",
              "gameObject": "Main Camera (UnityEngine.GameObject)",
              "name": "Main Camera",
              "hideFlags": "None"
            },
            "children": []
          }
        ]
      }
    ]
  },
  "components": [
    {
      "attributes": {
        "visible": {
          "value": "True",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "camera": {
          "value": "Main Camera (UnityEngine.Camera)",
          "type": "UnityEngine.Behaviour",
          "isWritable": "True"
        },
        "cullingDepth": {
          "value": "200",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "enableDepthCulling": {
          "value": "False",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "depthCulled": {
          "value": "False",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "didRenderThisFrame": {
          "value": "True",
          "type": "System.ValueType",
          "isWritable": "True"
        }
      },
      "name": "Main Camera (WaterSurfaceOnCamera)",
      "type": "WaterSurfaceOnCamera",
      "GameObject": "/EnvironmentCamera/Main Camera"
    },
    {
      "attributes": {
        "camera": {
          "value": "Main Camera (UnityEngine.Camera)",
          "type": "UnityEngine.Behaviour",
          "isWritable": "True"
        },
        "visible": {
          "value": "True",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "supportHDRTextures": {
          "value": "True",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "supportDX11": {
          "value": "False",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "isSupported": {
          "value": "True",
          "type": "System.ValueType",
          "isWritable": "True"
        }
      },
      "name": "Main Camera (WaterscapeVolumeOnCamera)",
      "type": "WaterscapeVolumeOnCamera",
      "GameObject": "/EnvironmentCamera/Main Camera"
    },
    {
      "attributes": {
        "shader": {
          "value": "Hidden/Waterscape/WaterSunShafts (UnityEngine.Shader)",
          "type": "UnityEngine.Object",
          "isWritable": "True"
        },
        "material": {
          "value": "Hidden/Waterscape/WaterSunShafts (UnityEngine.Material)",
          "type": "UnityEngine.Object",
          "isWritable": "True"
        },
        "maxDistance": {
          "value": "15",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "shaftsScale": {
          "value": "-0.16",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "startDistance": {
          "value": "5",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "reduction": {
          "value": "2",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "intensity": {
          "value": "3.93",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "traceStepSize": {
          "value": "0.05",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "consoleReduction": {
          "value": "4",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "consoleTraceStepSize": {
          "value": "0.1",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "eventsOnly": {
          "value": "True",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "supportHDRTextures": {
          "value": "True",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "supportDX11": {
          "value": "False",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "isSupported": {
          "value": "True",
          "type": "System.ValueType",
          "isWritable": "True"
        }
      },
      "name": "Main Camera (WaterSunShaftsOnCamera)",
      "type": "WaterSunShaftsOnCamera",
      "GameObject": "/EnvironmentCamera/Main Camera"
    },
    {
      "attributes": {
        "profile": {
          "value": "default_Post-FXProfile(Clone) (UnityEngine.PostProcessing.PostProcessingProfile)",
          "type": "UnityEngine.ScriptableObject",
          "isWritable": "True"
        },
        "jitteredMatrixFunc": {
          "value": "System.Func`2[UnityEngine.Vector2,UnityEngine.Matrix4x4]",
          "type": "System.MulticastDelegate",
          "isWritable": "True"
        },
        "m_CommandBuffers": {
          "value": "System.Collections.Generic.Dictionary`2[System.Type,System.Collections.Generic.KeyValuePair`2[UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer]]",
          "type": "System.Object",
          "isWritable": "True"
        },
        "m_Components": {
          "value": "System.Collections.Generic.List`1[UnityEngine.PostProcessing.PostProcessingComponentBase]",
          "type": "System.Object",
          "isWritable": "True"
        },
        "m_ComponentStates": {
          "value": "System.Collections.Generic.Dictionary`2[UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean]",
          "type": "System.Object",
          "isWritable": "True"
        },
        "m_MaterialFactory": {
          "value": "UnityEngine.PostProcessing.MaterialFactory",
          "type": "System.Object",
          "isWritable": "True"
        },
        "m_RenderTextureFactory": {
          "value": "UnityEngine.PostProcessing.RenderTextureFactory",
          "type": "System.Object",
          "isWritable": "True"
        },
        "m_Context": {
          "value": "UnityEngine.PostProcessing.PostProcessingContext",
          "type": "System.Object",
          "isWritable": "True"
        },
        "m_Camera": {
          "value": "Main Camera (UnityEngine.Camera)",
          "type": "UnityEngine.Behaviour",
          "isWritable": "True"
        },
        "m_PreviousProfile": {
          "value": "default_Post-FXProfile(Clone) (UnityEngine.PostProcessing.PostProcessingProfile)",
          "type": "UnityEngine.ScriptableObject",
          "isWritable": "True"
        },
        "m_RenderingInSceneView": {
          "value": "False",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "m_DebugViews": {
          "value": "UnityEngine.PostProcessing.BuiltinDebugViewsComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1[UnityEngine.PostProcessing.BuiltinDebugViewsModel]",
          "isWritable": "True"
        },
        "m_AmbientOcclusion": {
          "value": "UnityEngine.PostProcessing.AmbientOcclusionComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1[UnityEngine.PostProcessing.AmbientOcclusionModel]",
          "isWritable": "True"
        },
        "m_ScreenSpaceReflection": {
          "value": "UnityEngine.PostProcessing.ScreenSpaceReflectionComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1[UnityEngine.PostProcessing.ScreenSpaceReflectionModel]",
          "isWritable": "True"
        },
        "m_FogComponent": {
          "value": "UnityEngine.PostProcessing.FogComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1[UnityEngine.PostProcessing.FogModel]",
          "isWritable": "True"
        },
        "m_MotionBlur": {
          "value": "UnityEngine.PostProcessing.MotionBlurComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1[UnityEngine.PostProcessing.MotionBlurModel]",
          "isWritable": "True"
        },
        "m_Taa": {
          "value": "UnityEngine.PostProcessing.TaaComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.AntialiasingModel]",
          "isWritable": "True"
        },
        "m_EyeAdaptation": {
          "value": "UnityEngine.PostProcessing.EyeAdaptationComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.EyeAdaptationModel]",
          "isWritable": "True"
        },
        "m_DepthOfField": {
          "value": "UnityEngine.PostProcessing.DepthOfFieldComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.DepthOfFieldModel]",
          "isWritable": "True"
        },
        "m_Bloom": {
          "value": "UnityEngine.PostProcessing.BloomComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.BloomModel]",
          "isWritable": "True"
        },
        "m_ChromaticAberration": {
          "value": "UnityEngine.PostProcessing.ChromaticAberrationComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.ChromaticAberrationModel]",
          "isWritable": "True"
        },
        "m_ColorGrading": {
          "value": "UnityEngine.PostProcessing.ColorGradingComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.ColorGradingModel]",
          "isWritable": "True"
        },
        "m_UserLut": {
          "value": "UnityEngine.PostProcessing.UserLutComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.UserLutModel]",
          "isWritable": "True"
        },
        "m_Grain": {
          "value": "UnityEngine.PostProcessing.GrainComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.GrainModel]",
          "isWritable": "True"
        },
        "m_Vignette": {
          "value": "UnityEngine.PostProcessing.VignetteComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.VignetteModel]",
          "isWritable": "True"
        },
        "m_Dithering": {
          "value": "UnityEngine.PostProcessing.DitheringComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.DitheringModel]",
          "isWritable": "True"
        },
        "m_Fxaa": {
          "value": "UnityEngine.PostProcessing.FxaaComponent",
          "type": "UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1[UnityEngine.PostProcessing.AntialiasingModel]",
          "isWritable": "True"
        },
        "m_ComponentsToEnable": {
          "value": "System.Collections.Generic.List`1[UnityEngine.PostProcessing.PostProcessingComponentBase]",
          "type": "System.Object",
          "isWritable": "True"
        },
        "m_ComponentsToDisable": {
          "value": "System.Collections.Generic.List`1[UnityEngine.PostProcessing.PostProcessingComponentBase]",
          "type": "System.Object",
          "isWritable": "True"
        }
      },
      "name": "Main Camera (UnityEngine.PostProcessing.PostProcessingBehaviour)",
      "type": "UnityEngine.PostProcessing.PostProcessingBehaviour",
      "GameObject": "/EnvironmentCamera/Main Camera"
    },
    {
      "attributes": {
        "overlayShader": {
          "value": "Hidden/MainMenuOverlay (UnityEngine.Shader)",
          "type": "UnityEngine.Object",
          "isWritable": "True"
        },
        "fadeTime": {
          "value": "1",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "overlayFadeValue": {
          "value": "1",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "mainFadeValue": {
          "value": "1",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "overlayMask": {
          "value": "UnityEngine.LayerMask",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "overlayMaterial": {
          "value": "Hidden/MainMenuOverlay (UnityEngine.Material)",
          "type": "UnityEngine.Object",
          "isWritable": "True"
        },
        "targetTexture": {
          "value": " (UnityEngine.RenderTexture)",
          "type": "UnityEngine.Texture",
          "isWritable": "True"
        },
        "camera": {
          "value": "Main Camera (UnityEngine.Camera)",
          "type": "UnityEngine.Behaviour",
          "isWritable": "True"
        },
        "renderingOverlay": {
          "value": "False",
          "type": "System.ValueType",
          "isWritable": "True"
        }
      },
      "name": "Main Camera (MenuSceneFadeIn)",
      "type": "MenuSceneFadeIn",
      "GameObject": "/EnvironmentCamera/Main Camera"
    },
    {
      "attributes": {
        "defaultProfile": {
          "value": "default_Post-FXProfile (UnityEngine.PostProcessing.PostProcessingProfile)",
          "type": "UnityEngine.ScriptableObject",
          "isWritable": "True"
        },
        "dofAdaptationSpeed": {
          "value": "50",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "aboveWaterDofFocalLength": {
          "value": "42",
          "type": "System.ValueType",
          "isWritable": "True"
        },
        "underWaterDofFocalLength": {
          "value": "42",
          "type": "System.ValueType",
          "isWritable": "True"
        }
      },
      "name": "Main Camera (UwePostProcessingManager)",
      "type": "UwePostProcessingManager",
      "GameObject": "/EnvironmentCamera/Main Camera"
    }
  ]
};
