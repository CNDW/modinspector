import Service from '@ember/service';
import Socket from 'simple-websocket';
import { inject as service } from '@ember/service';
import data from './data';

const TARGET = 'instance';

export default Service.extend({
  socket: null,
  socketHandler: service('socket-handler'),
  isInitialized: false,
  store: service(),

  init(...args) {
    this._super(...args);
    const socket = new Socket('ws://localhost:40510');
    this.set('socket', socket);
    socket.on('connect', () => this.onConnect());
    socket.on('data', (raw) => this.onData(raw));

    // this.socketHandler.execute('scene', data.scene);
    // this.socketHandler.execute('component', data.components);
  },

  send(command, payload = {}) {
    this.socket.send(JSON.stringify([`${TARGET}:${command}`, payload]));
  },

  onConnect() {
    this.socket.send(JSON.stringify(['register', { user: 'inspector' }]));
    this.set('isInitialized', true);
  },

  onData(raw) {
    try {
      const [command, payload] = JSON.parse(raw.toString());
      console.log(`Recieved command from instance: ${command}`);
      console.log(payload);
      this.socketHandler.execute(command, payload);
    } catch (err) {
      console.warn('Unable to parse data from incoming socket');
      console.error(err);
      console.warn(raw.toString());
    }
  }
});
