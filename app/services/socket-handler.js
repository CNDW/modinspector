import Service from '@ember/service';
import { inject as service } from '@ember/service';

export default Service.extend({
  store: service('store'),

  execute(command, payload) {
    if (!this.actions[command]) {
      console.log(`unhandled socket command '${command}'`);
      return;
    }

    this.actions[command].call(this, payload);
  },

  actions: {
    scene(payload) {
      const sceneSerializer = this.store.serializerFor('scene');
      const gameObjectSerializer = this.store.serializerFor('game-object');

      const scene = sceneSerializer.fromSocket(payload);
      sceneSerializer.pushPayload(this.store, { data: scene });
      gameObjectSerializer.pushPayload(this.store, { data: gameObjectSerializer.fromSceneSocket(payload) });

      const model = this.store.peekRecord('scene', scene.id);
      model.set('isRefreshing', false);
    },

    gameObject(payload) {
      const gameObjectSerializer = this.store.serializerFor('game-object');
      const gameObject = gameObjectSerializer.fromSocket(payload);
      gameObjectSerializer.pushPayload(this.store, { data: gameObject });

      const model = this.store.peekRecord('game-object', gameObject.id);
      model.set('isRefreshing', false);
    },

    components(payload) {
      const serializer = this.store.serializerFor('game-component');
      serializer.pushPayload(this.store, { data: payload.map(p => serializer.fromSocket(p)) });
    },

    transform(payload) {
      const serializer = this.store.serializerFor('transform');
      serializer.pushPayload(this.store, { data: payload });
    }
  }
});
