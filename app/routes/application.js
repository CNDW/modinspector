import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { observer, computed } from '@ember/object';

export default Route.extend({
  websocket: service(),

  wsInitializeResolve: null,

  scene: computed(function() {
    return this.store.peekAll('scene');
  }),

  model() {
    return new Promise((res) => {
      if (this.get('websocket.isInitialized')) {
        res(this.scene);
      } else {
        this.set('wsInitializeResolve', () => res(this.scene));
      }
    });
  },

  isInitialized: observer('websocket.isInitialized', function() {
    if (this.wsInitializeResolve && this.get('websocket.isInitialized')) {
      this.wsInitializeResolve();
      this.set('wsInitializeResolve', null);
    }
  })
});
