import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  websocket: service(),

  model(params) {
    return this.store.peekRecord('game-object', params.object_id);
  },

  afterModel(model) {
    if (!model) {
      this.transitionTo('application');
    }

    this.websocket.send('getGameObject', { path: model.id });
  }
});
