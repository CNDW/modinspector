import Component from '@ember/component';

import { computed } from '@ember/object';

export default Component.extend({
  toggled: false,
  disabled: false,

  classNameBindings: ['disabled'],
  classNames: ['toggle-button'],

  click() {
    this.toggleProperty('toggled');
  },

  icon: computed('toggled', function() {
    if (this.disabled) {
      return 'circle';
    }
    return this.toggled ? 'minus-circle' : 'plus-circle';
  })
});
