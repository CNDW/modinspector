import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: "card col-12",

  fields: computed('model.fields', function() {
    const { fields } = this.model;
    if (!fields) {
      return [];
    } else {

      return Object.keys(fields).map(k => ({
        name: k,
        value: fields[k].value,
        type: fields[k].type
      }));
    }
  })
});
