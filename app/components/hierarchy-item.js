import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  showNestedContent: true,
  websocket: service(),

  actions: {
    getScene() {
      this.model.set('isRefreshing', true);
      this.websocket.send('getScene', { path: this.model.id });
    }
  }
});
